const { io } = require("../server");

io.on("connection", (client) => {
  console.log("Usuario conectado");

  client.emit("enviarMensaje", {
    usuario: "Administracion",
    mensaje: "Bienvenido",
  });

  //   Verificar si un usuario se desconecta
  client.on("disconnect", () => {
    console.log("Usuario desconectado");
  });

  //   Escuchar al cliente
  client.on("enviarMensaje", (data, callback) => {
    console.log(data);

    client.broadcast.emit("enviarMensaje", data);

    // if (message.usuario) {
    //   callback({
    //     res: "Todo salio bien",
    //   });
    // } else {
    //   callback({
    //     res: "Todo salio mal !!",
    //   });
    // }
  });
});
